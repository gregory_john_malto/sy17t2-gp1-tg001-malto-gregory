﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace suikoden_Duel
{
    class Program
    {
        static void Main(string[] args)
        {

            int hansInt;
            int PD, PDmax, Php;
            int HD, HDmax, Hhp;

            Random rnd = new Random();


            Console.WriteLine("Enter hero hp =");
            Php = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter min Player Damage =");
            PD = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter max Player Damage =");
            PDmax = Convert.ToInt32(Console.ReadLine());
            Console.Clear();
            //////////////////////////////////////////////////////////////////////// hans stats
            Console.WriteLine("Enter hans hp =");
            Hhp = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter min Hans Damage =");
            HD = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter max Hans Damage =");
            HDmax = Convert.ToInt32(Console.ReadLine());
            Console.Clear();
            ///////////////////////////////////////////Battle

            while (Php >= 0 || Hhp >= 0)
            {
                hansInt = rnd.Next(1, 4);
                Console.WriteLine("Player HP: {0} \nEnemy HP: {1}", Php, Hhp);
                Console.WriteLine("Choose Command: \n1= Attack \n2= Defend \n3= Wild Attack \nEnter your command: ");
                int Pinput = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("You chose: {0} \nThe opponent chose: {1}", Pinput, hansInt);
                /////attack     PlayerFinalDamage = pdf ,,,,,,,,,,,,,,,,, HansFInalDamage = HFD
                int PFD = rnd.Next(PD, PDmax);
                int HFD = rnd.Next(HD, HDmax);

                if (hansInt == 1)
                {
                    if (Pinput == 1)
                    {

                        Php -= HFD;
                        Hhp -= PFD;

                    }
                    else if (Pinput == 2)
                    {
                        int newHFD = HFD / 2;
                        Console.WriteLine(" blocked! \nhans attack for {0} damage.", PFD);
                        Php -= HFD;

                    }
                    else if (Pinput == 3)
                    {
                        int newPFD = PFD * 2;
                        Console.WriteLine("The enemy critically hits for {0} damage. \nYou attack for {1} damage.", PFD, HFD);
                        Php -= HFD;
                        Hhp -= PFD;

                    }
                }
                ///////////////////////////////////////////////////////////// battle 2
                if (hansInt == 2)
                {
                    if (Pinput == 1)
                    {
                        int newPFD = PFD / 2;
                        Console.WriteLine("hans defended your attack");
                        Hhp -= PFD;

                    }
                    else if (Pinput == 2)
                    {

                        Console.WriteLine(" both defended!");


                    }
                    else if (Pinput == 3)
                    {
                        int newHFD = HFD * 2;
                        Console.WriteLine("The enemy critically hits for {0} damage. \nYou attack for {1} damage.", PFD, HFD);
                        Php -= HFD;
                        Hhp -= PFD;

                    }
                }
                ////////////////////////////////////////battle 3
                if (hansInt == 3)  // wild attack
                {
                    if (Pinput == 1)
                    {

                        int newPFD = PFD * 2;
                        Console.WriteLine("The enemy critically hits for {0} damage. \nYou attack for {1} damage.", PFD, HFD);
                        Php -= HFD;
                        Hhp -= PFD;

                    }
                    else if (Pinput == 2)
                    {
                        int newPFD = PFD / 2;
                        Console.WriteLine(" blocked! \nPlayer attack for {0} damage.", PFD);
                        Hhp -= PFD;

                    }
                    else if (Pinput == 3)
                    {
                        int newPFD = PFD * 2;
                        int newHFD = HFD * 2;
                        Console.WriteLine("The enemy critically hits for {0} damage. \nYou attack for {1} damage.", PFD, HFD);
                        Php -= HFD;
                        Hhp -= PFD;
                        Console.WriteLine("Your HP is {0} \nEnemy HP is {1}", Php, Hhp);
                    }
                }
                if (Php <= 0 && Hhp >= 0)
                {
                    Console.WriteLine(" Hans has slain you! Game over");
                }

                if (Php >= 0 && Hhp <= 0)
                {
                    Console.WriteLine(" You have slain hans! You won!");
                }
                Console.ReadKey();
                Console.Clear();
                
                
            }
        }
    }
}

