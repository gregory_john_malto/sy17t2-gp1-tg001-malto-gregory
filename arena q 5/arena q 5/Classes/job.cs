﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace arena
{
    class job
    {

        static Random r = new Random();

        public string jobname;
        public int maxHp = 10;
        public int hp = 10;
        public int pow = 5;//damage
        public int vit = 5; //defense (pow of attacker - vit of defender)* bonus damage
        public int agi = 5;
        public int dex = 5;


        public int showHealth()
        {
            if (hp < 0)
            {
                return 0;
            }
            else
            {
                return hp;
            }
        }

        public void print()
        {
            Console.WriteLine("hp: " + hp);
            Console.WriteLine("pow: " + pow);
            Console.WriteLine("vit: " + vit);
            Console.WriteLine("agi: " + agi);
            Console.WriteLine("dex: " + dex);
        }
        public int classid;
        public int jweakness;

        public int damageCalculation(job defender)
        {
            double damage;
            damage = this.pow - defender.vit;
            if (damage <= 0)
            {
                damage = 1;
            }

            if (defender.jweakness == this.classid)
            {
                damage = damage * 1.5;
            }

            return Convert.ToInt32(damage);

        }

        public int hitCalculation(job defender)
        {
            double hitRate;
            hitRate = this.dex / defender.agi * 100;
            if (hitRate < 20)
            {
                hitRate = 20;
            }
            else if (hitRate > 80)
            {
                hitRate = 80;
            }

            return Convert.ToInt32(hitRate);

        }

        public int hitOrMiss(int damage, int hitChance)
        {
            int hitRoll = r.Next(1, 100);
            if (hitRoll > hitChance)
            {
                Console.WriteLine("a miss!");
                return 0;
            }
            else
            {
                Console.WriteLine("it hits!");
                return damage;
            }
        }


        public void Attack(job Target)
        {
            
            damageCalculation(Target);
            hitCalculation(Target);
            Target.hp -= Target.hitOrMiss(this.damageCalculation(Target), this.hitCalculation(Target));

            
       


        // compute if it will hit
        // compute for bonus damage
        // clamp damage value
           // apply damage
        }

    }

}

