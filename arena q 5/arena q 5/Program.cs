﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace arena
{
    class Program
    {
        static Random r = new Random();


        static void Main(string[] args)
        {
            job player = new job();
            job enemy = new job();

            Console.WriteLine("welcome to sword and sandals");
            Console.WriteLine("gladiators gladiators GLADIATORS");

            Console.WriteLine("hello gladiator what is your name ?:");
            Console.ReadLine();
            Console.WriteLine("choose you class 1 for warrior , 2 for mage , 3 for assassin");
            int input = Convert.ToInt32(Console.ReadLine());
            if (input == 1)
            {
                player = new warrior();
            }
            else if (input == 2)
            {
                player = new mage();
            }
            else if (input == 3)
            {
                player = new assasin();
            }
            Console.WriteLine("You are now a {0}", player.jobname);
            Console.WriteLine("Your stats are : ");
            player.print();
            Console.ReadLine();

            //////////////////////////////////////////
            int battleCount = 0;
            while (battleCount < 10)
            {

                int enemyjob = r.Next(1, 3);
                if (enemyjob == 1)
                {
                    enemy = new warrior();
                    enemy.hp += battleCount;
                    enemy.maxHp += battleCount;
                }
                else if (enemyjob == 2)
                {
                    enemy = new mage();
                    enemy.pow += battleCount;
                }
                else if (enemyjob == 3)
                {
                    enemy = new assasin();
                    enemy.agi += battleCount;
                    enemy.dex += battleCount;

                }
                Console.WriteLine("Your enemy is a {0}", enemy.jobname);
                Console.WriteLine("Your enemy's stats are : ");
                enemy.print();
                Console.ReadLine();
                Console.WriteLine("");
                Console.WriteLine("Let the great battle commence!");
                Console.WriteLine("Press any key...");
                Console.ReadKey();
                Console.Clear();

                while (player.hp > 0 && enemy.hp > 0)
                {
                    if (enemy.agi > player.agi)
                    {
                        enemy.Attack(player);
                        Console.WriteLine("Player hp: {0} \n", player.showHealth());
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        if (player.hp <= 0)
                        {
                            break;
                        }
                        player.Attack(enemy);
                        //enemy.hp -= player.hitOrMiss(player.damageCalculation(enemy), player.hitCalculation(player));
                        Console.WriteLine("Enemy hp: {0} \n", enemy.showHealth());
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                    }
                    else
                    {
                        player.Attack(enemy);
                        //enemy.hp -= player.hitOrMiss(player.damageCalculation(enemy), player.hitCalculation(player));
                        Console.WriteLine("Enemy hp: {0}\n", enemy.showHealth());
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        if (enemy.hp <= 0)
                        {
                            break;
                        }
                        enemy.Attack(player);
                       // player.hp -= enemy.hitOrMiss(enemy.damageCalculation(player), enemy.hitCalculation(player));
                        Console.WriteLine("Player hp: {0}\n", player.showHealth());
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                    }

                }
                if (player.hp > 0)// player wins
                {
                    battleCount++;
                    if (enemy.classid == 1)
                    {
                        player.maxHp += 3;
                        player.hp += 3;
                        player.vit += 3;
                        player.print();
                    }
                    else if (enemy.classid == 2)
                    {
                        player.dex += 3;
                        player.agi += 3;
                        player.print();
                    }
                    else if (enemy.classid == 3)
                    {
                        player.pow += 5;
                        player.print();
                    }

                    player.hp += player.hp / 3;
                    if (player.hp > player.maxHp)
                    {
                        player.hp = player.maxHp;
                    }
                    Console.WriteLine("You won! ...");
                    Console.WriteLine("The victory has  reinvigorated your body!");
                    Console.WriteLine("Your new stats are :");
                    player.print();
                    Console.WriteLine("Stronger enemies await...");
                    Console.ReadKey();
                    Console.Clear();

                }
                else
                {
                    Console.WriteLine("game over!");
                    break;
                }
            }
        }
    }
}
