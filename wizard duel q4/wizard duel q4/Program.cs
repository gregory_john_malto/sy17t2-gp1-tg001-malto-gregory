﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wizard_duel
{

    class Wizard : Spell
    {
        public string job;
        public int hp;
        public int mana;
        public Wizard()/// req. constructor
        {
            job = "wizard";
            hp = 10;
            mana = 10;
        }
        public void print()
        {
            Console.WriteLine(" hp:" + hp);
            Console.WriteLine(" mana:" + mana);
        }
    }

    class Spell
    {
        public int fireballD = 2;
        public int manaC = 2;
    }
    class Program
    {
        static void Main(string[] args)
        {
            Wizard player = new Wizard();
            Console.WriteLine("player :" + player.job);
            player.print();
            ///////////////////////////////////////////////////
            Wizard enemy = new Wizard();
            Console.WriteLine("enemy :" + enemy.job);
            enemy.print();
            Console.ReadKey();


            while (player.hp >= 0 && enemy.hp >= 0 && player.mana >= 0 && enemy.mana >= 0)
            {
                Console.WriteLine("Player HP: {0} \nEnemy HP: {1}", player.hp, enemy.mana);
                Console.WriteLine("Player Mana: {0} \nEnemy Mana: {1}", player.mana, enemy.mana);
                player.mana -= player.manaC;
                enemy.hp -= player.fireballD;
                ///////////////////////////////////////////////////////
                enemy.mana -= enemy.manaC;
                player.hp -= enemy.fireballD;
                Console.ReadKey();
            }


        }
    }
}
